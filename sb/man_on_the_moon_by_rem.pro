{t: Man On The Moon by R.E.M.}
{st: Floater}
{key: [C]}

ONE GUITAR [C D C / | C D C /]
ALL GUITARS [C]Mott the Hoople and the [D]Game of Life
[C]Yeah yeah yeah [/]yeah
[C]Andy Kaufman in the [D]wrestling match
[C]Yeah yeah yeah [/]yeah
[C]Monopoly, twenty-one [D]checkers, and chess
[C]Yeah yeah yeah [/]yeah
[C]Mister Fred Blassie in a [D]breakfast mess
[C]Yeah yeah yeah [/]yeah
[C]Let's play Twister, [D]let's play Risk
[C]Yeah yeah yeah [/]yeah
[C]See you in Heaven if you [D]make the list
[C]Yeah yeah yeah [/]yeah

Now, [Am]Andy, did you hear about [G]this one?
[Am]Tell me, are you locked in the [G]punch?
[Am]Andy, are you goofing on [G]Elvis?
Hey [C]baby, [D]are we losing [/]touch?
{soc}
[G]If you be[Am]lieved [C]they put [_*Bm_]a man on the [G]moon
[Am]Man on the [D]moon
[G]If you be[Am]lieve [C]there's nothing [_*Bm_]up his [Am]sleeve
Then nothing is [Am]cool
{eoc}
[C]Moses went walking with the [D]staff of wood
[C]Yeah yeah yeah [/]yeah
[C]Newton got beaned by the [D]apple good
[C]Yeah yeah yeah [/]yeah
[C]Egypt was troubled by the [D]horrible asp
[C]Yeah yeah yeah [/]yeah
[C]Mister Charles Darwin had the [D]gall to ask
[C]Yeah yeah yeah [/]yeah

Now, [Am]Andy, did you hear about [G]this one?
[Am]Tell me, are you locked in the [G]punch?
[Am]Andy, are you goofing on [G]Elvis?
Hey [C]baby, [D]are you having [/]fun?
{soc}
[G]If you be[Am]lieved [C]they put [_*Bm_]a man on the [G]moon
[Am]Man on the [D]moon
[G]If you be[Am]lieve [C]there's nothing [_*Bm_]up his [Am]sleeve
Then nothing is [Am]cool
{eoc}
SOLO [Em D | Em D | Em D | D]
BREAK IT DOWN [C*]Here's a little agit for the [D*]never-believer
[C*]Yeah yeah yeah [/]yeah
[C*]Here's a little ghost for the [D*]offering
[C*]Yeah yeah yeah [/]yeah
BRING IT BACK [C]Here's a truck stop in[D]stead of Saint Peter's
[C]Yeah yeah yeah [/]yeah
[C]Mister Andy Kaufman's gone [D]wrestling
[C]Yeah yeah yeah [/]yeah

Now, [Am]Andy, did you hear about [G]this one?
[Am]Tell me, are you locked in the [G]punch?
[Am]Andy, are you goofing on [G]Elvis?
Hey [C]baby, [D]are we losing [/]touch?
{soc}
[G]If you be[Am]lieved [C]they put [_*Bm_]a man on the [G]moon
[Am]Man on the [D]moon
[G]If you be[Am]lieve [C]there's nothing [_*Bm_]up his [Am]sleeve
Then nothing is [Am]cool

[G]If you be[Am]lieved [C]they put [_*Bm_]a man on the [G]moon
[Am]Man on the [D]moon
[G]If you be[Am]lieve [C]there's nothing [_*Bm_]up his [Am]sleeve
Then nothing is [Am]cool

[G]If you be[Am]lieved [C]they put [_*Bm_]a man on the [G]moon
[Am]Man on the [D]moon
[G]If you be[Am]lieve [C]there's nothing [_*Bm_]up his [Am]sleeve
Then nothing is [Am]cool [Em*]
{eoc}


