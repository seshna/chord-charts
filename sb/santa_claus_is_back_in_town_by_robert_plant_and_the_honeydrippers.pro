{t: Santa Claus Is Back In Town by Robert Plant And The Honey Drippers (3)}
{st: Floater}
{key: [G]}

ONE GUITAR [G]CHRISTMAS, [C]CHRISTMAS, [G]CHRISTMAS
ALL GUITARS [D^]
Well, it's [G]Christmas time pretty baby
[C]And the snow is fallin' on the [G]ground
CHRISTMAS, [G]CHRISTMAS
Yes it's [C]Christmas time pretty baby
[C]And the snow is falling on the [G]ground
CHRISTMAS, [G]CHRISTMAS
{soc}
Well, you be a [D]real good mama baby 
[C]'cause Santa Claus is back in [G]town
CHRISTMAS, [G]CHRISTMAS
{eoc}
[G*]Got no sleigh with reindeer, [G*]no pack on my back
[G*]You will see me comin' in a [G^]big black Cadillac
Whoa [C]oh, Christmas time pretty baby
[C]And the snow is fallin' on the [G]ground
CHRISTMAS, [G]CHRISTMAS
{soc}
You be a [D]real good lookin baby
[C]'cause Santa Claus is back in [G]town
CHRISTMAS, [G]CHRISTMAS
{eoc}
ONE GUITAR [G]CHRISTMAS, [C]CHRISTMAS, [G]CHRISTMAS
ALL GUITARS [G^]
SOLO [C / | G / | D C | G / |
     | G C | G / | C / | G / | D C | G /]

ONE GUITAR [G]CHRISTMAS, [C]CHRISTMAS, [G]CHRISTMAS
ALL GUITARS [G^]
SOLO [C / | G / | D C | G /]

[G*]Got no sleigh with reindeer, [G*]no pack on my back
[G*]You will see me comin' in a [G^]big black Cadillac
Whoa [C]oh, Christmas time pretty baby
[C]And the snow is fallin' on the [G]ground
CHRISTMAS, [G]CHRISTMAS
{soc}
You be a [D]real good lookin baby
[C]'cause Santa Claus is back in [G]town
CHRISTMAS, [G]CHRISTMAS
{eoc}
CHRISTMAS, CHRISTMAS
He's back in town, he's back in town

