{t: God's Gonna Cut You Down}
{st: Johnny Cash}
{capo: 1}
{key: [Am]}

{sog}
FOOT STOMPS || Am C Am | G Am C Am | Am C | C Dm Em C |
{eog}
{soc}
You can [Am]run on for a [/]long time, [Am]run on for a [/]long time
[Am]Run on for a [/]long time
[C]Sooner or [Am]later God'll [G]cut you [Am]down
[C]Sooner or [Dm]later God'll [Em]cut you [Am]down [/]
Go [Am]tell that long tongue [/]liar
Go and [Am]tell that midnight [/]rider
Tell the [Am]rambler, the gambler, the [/]back biter
[C]Tell 'em that [Am]God's gonna [G]cut 'em [Am]down
[C]Tell 'em that [Dm]God's gonna [Em]cut 'em [Am]down [/ | Am /]
{eoc}
Well [Am]my goodness gracious let me [/]tell you the news
My [Am]head's been wet with the [/]midnight dew
[Am]I've been down on [/]bended knee
[Am]Talkin' to the man from [/]Galilee
He [Am]spoke to me in the [/]voice so sweet
I [Am]thought I heard the shuffle of the [/]angel's feet
He [Am]called my name and my [/]heart stood still
When [Am*]he said, "John, go do my [C*]will!"
{soc}
Go [Am]tell that long tongue [/]liar
Go and [C/E]tell that midnight [/]rider
Tell the [Am]rambler, the gambler, the [/]back biter
[C]Tell 'em that [Am]God's gonna [G]cut 'em [Am]down
[C]Tell 'em that [Dm]God's gonna [Em]cut 'em [Am]down [/]
You can [Am]run on for a [/]long time, [Am]run on for a [/]long time
[Am]Run on for a [/]long time
[C]Sooner or [Am]later God'll [G]cut you [Am]down
[C]Sooner or [Dm]later God'll [Em]cut you [Am]down [/]
{eoc}
Well you may [Am]throw your rock and [/]hide your hand
[Am]Workin' in the dark against your [/]fellow man
But as [Am]sure as God made [/]black and white
What's [Am]down in the dark will be [/]brought to the light
{soc}
You can [Am]run on for a [/]long time, [Am]run on for a [/]long time
[Am]Run on for a [/]long time
[C]Sooner or [Am]later God'll [G]cut you [Am]down
[C]Sooner or [Dm]later God'll [Em]cut you [Am]down [/]
Go [Am]tell that long tongue [/]liar
Go and [Am]tell that midnight [/]rider
Tell the [Am]rambler, the gambler, the [/]back biter
[C]Tell 'em that [Am]God's gonna [G]cut 'em [Am]down
[C]Tell 'em that [Am]God's gonna [G]cut 'em [Am]down
[C]Tell 'em that [Dm]God's gonna [Em]cut 'em [Am*]down
{eoc}

