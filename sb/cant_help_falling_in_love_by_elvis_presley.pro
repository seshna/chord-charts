#+TITLE:Can't Help Falling In Love by Elvis Presley
#+BOOK:
#+PAGE: 
#+OPTIONS: \n:t toc:nil num:nil ^:nil f:nil *:nil ':nil p:nil author:nil
#+EXPORT_FILE_NAME: _export
#+HTML_HEAD_EXTRA: <style>.chorus{margin-left: 1em;} body{font-family: 'Courier New' !important;}</style>

* Chord Chart
[C G | C G]  
[C]Wise [Em]men [Am]say, [/]only [F]fools [C/E]rush [G]in [/]
But [F]I [G]can't [Am]help [Dm]falling in [C/G]love [G]with [C]you [/]

[C]Shall [Em]I [Am]stay? [/]Would it [F]be [C/E]a [G]sin [/]
If [F]I [G]can't [Am]help [Dm]falling in [C/G]love [G]with [C]you? [/]

[Em]Like a river [B7]flows, [Em]surely to the [B7]sea
[Em]Darling so it [B7]goes
[Em]Some things [A7]are meant to [Dm]be [G]

[C]Take [Em]my [Am]hand, [/]take my [F]whole [C/E]life [G]too [/]
For [F]I [G]can't [Am]help [Dm]falling in [C/G]love [G]with [C]you [/]

[Em]Like a river [B7]flows, [Em]surely to the [B7]sea
[Em]Darling so it [B7]goes
[Em]Some things [A7]are meant to [Dm]be [G]

[C]Take [Em]my [Am]hand, [/]take my [F]whole [C/E]life [G]too [/]
For [F]I [G]can't [Am]help [Dm]falling in [C/G]love [G]with [C]you [/]
For [F]I [G]can't [Am]help [Dm]falling in [C/G*]love [G*]with [C*]you

* Lyrics
  
Wise men say, only fools rush in
But I can't help falling in love with you
Shall I stay?
Would it be a sin
If I can't help falling in love with you?
Like a river flows
Surely to the sea
Darling, so it goes
Some things are meant to be
Take my hand
Take my whole life too
For I can't help falling in love with you
Like a river flows
Surely to the sea
Darling, so it goes
Some things are meant to be
Take my hand
Take my whole life too
For I can't help falling in love with you
For I can't help falling in love with you
