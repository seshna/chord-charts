#+title: Get It On by T Rex
#+roam_tags: chord_charts
#+BOOK: n
#+PAGE:
#+KEY: E
#+TIME_SIGNATURE: 4/4
#+OPTIONS: \n:t toc:nil num:nil ^:nil f:nil *:nil ':nil p:nil author:nil
#+EXPORT_FILE_NAME: _export
#+HTML_HEAD_EXTRA: <style>.chorus{margin-left: 1em;} body{font-family: 'Courier New' !important;}</style>

* Chord Chart
ONE GUITAR [E / | E /] ALL GUITARS [E / | E /]
Well you're [E]dirty and sweet Clad in [A]black
Don't look back, and I [E]love you
You're [A]dirty and sweet oh [E]yea
Well you're [E]slim and you're weak
You got the [A]teeth of the Hydra u[E]pon you
You're [A]dirty sweet and you're my [E]girl [\]

Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E / | E /]

Well you're [E]built like a car
You got a [A]hubcap diamond star [E]halo
You're [A]built like a car, oh [E]yeah

You're an [E]untamed youth
That's the [A]truth, with your cloak full of [E]eagles
You're [A]dirty sweet and you're my [E]girl

Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E / | E /]

Well you're [E]windy and wild
You got the [A]blues in your shoes and your [E]stockings
You're [A]windy and wild, oh [E]yeah

Well you're [E]built like a car
You got a [A]hubcap diamond star [E]halo
You're [A]dirty sweet and you're my [E]girl

Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E / | E /]

ONE GUITAR [E / | E /] ALL GUITARS [E / | E /]

Well you're [E]dirty and sweet Clad in [A]black
Don't look back, and I love [E]you
You're [A]dirty and sweet oh [E]yea

Well you [E]dance when you walk
So lets [A]dance take a chance under[E]stand you
You're [A]dirty sweet and you're my [E]girl

Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E /] GET IT ON

ONE GUITAR [E / | E /] SOLO [E / | E /]

Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E /]
Get it [G^]on, bang a [A^]gong, get it on [E /] take me

SOLO [G^ A^ E / | E /]
But [E]meanwhile, I'm [\]still thinking of [E]you [\]

[E E*]

* Tab
    E
e|-----------------|-----------------|
B|-----------------|-----------------|
G|-----------------|-----------------|
D|-----------------|-----------------|
A|-2-2-4---2-4---2-|-2-2-4---2-4---2-|
E|-0-0-0-0-0-0-0-0-|-0-0-0-0-0-0-0-0-|
   1 + 2 + 3 + 4 +   1 + 2 + 3 + 4 +

* Lyrics
