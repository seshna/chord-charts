#+TITLE: Jolene by Dolly Parton (4)
#+BOOK: 7
#+PAGE: 37
#+OPTIONS: \n:t toc:nil num:nil ^:nil f:nil *:nil ':nil p:nil author:nil
#+EXPORT_FILE_NAME: _export
#+HTML_HEAD_EXTRA: <style>.chorus{margin-left: 1em;} body{font-family: 'Courier New' !important;}</style>

* Chord Chart
* Lyrics
  
Jolene, Jolene Jolene Jolene 
I'm begging of you please don't take my man 
Jolene, Jolene, Jolene, Jolene 
Please don't take him just because you can 

Your beauty is beyond compare
With flaming locks of auburn hair
With ivory skin and eyes of emerald green 
Your smile is like a breath of spring
Your voice is soft like summer rain
And I cannot compete with you, Jolene 

He talks about you in his sleep
There's nothing I can do to keep
From crying when he calls your name Jolene 
And I can easily understand
How you could easily take my man
But you don't know what he means to me
Jolene 

Jolene, Jolene, Jolene, Jolene 
I'm begging of you please don't take my man 
Jolene, Jolene, Jolene, Jolene 
Please don't take him just because you can 

You could have your choice of men
But I could never love again
He's the only one for me, Jolene 
I had to have this talk with you
My happiness depends on you
Whatever you decide to do, Jolene 

Jolene, Jolene Jolene, Jolene 
I'm begging of you please don't take my man 
Jolene, Jolene, Jolene, Jolene 
Please don't take him even though you can 
Jolene 

