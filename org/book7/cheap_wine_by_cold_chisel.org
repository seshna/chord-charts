#+TITLE: Cheap Wine by Cold Chisel (3)
#+BOOK: 7
#+PAGE: 16
#+OPTIONS: \n:t toc:nil num:nil ^:nil f:nil *:nil ':nil p:nil author:nil
#+EXPORT_FILE_NAME: _export
#+HTML_HEAD_EXTRA: <style>.chorus{margin-left: 1em;} body{font-family: 'Courier New' !important;}</style>

* Chord Chart
  
-----  
#+begin_chorus

#+end_chorus
-----

* Lyrics
Once I smoked a Danneman cigar drove a foreign car
Baby that was years ago I left it all behind
Had a friend, I heard she died
On a needle she was crucified
Baby that was years ago i left it all behind for my

Cheap wine and a three day growth
Cheap wine and a three day growth
Come on, come on, come on 

I don't mind taking charity from those that i despise
Baby I don't need your love 
I don't need your love
Baby you can shout at me, but you can't meet my eyes
I don't really need your love  NEED YOUR LOVE
I don't need your love, I got my

Cheap wine and a three day growth
Cheap wine and a three day growth
Come on, come on, come on, come one 

Sittin' on the beach drinkin' rocket fuels oh yeah 
Spent the whole night breakin' all the rules oh yeah 
Mending every minute of the day before
Watching the ocean, watching the shore
Watching the sunrise and
Thinking there could never be more, never be more, yeah

Anytime you wanna find me I don't have a telephone
I'm another world away but i always feel at home with my

Cheap wine and a three day growth
Cheap wine and a three day growth
Come on, come on, come on, come on   
