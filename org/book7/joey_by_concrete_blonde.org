#+TITLE: Joey by Concrete Blonde
#+BOOK: 7
#+PAGE: 36
#+OPTIONS: \n:t toc:nil num:nil ^:nil f:nil *:nil ':nil p:nil author:nil
#+EXPORT_FILE_NAME: _export
#+HTML_HEAD_EXTRA: <style>.chorus{margin-left: 1em;} body{font-family: 'Courier New' !important;}</style>

* Chord Chart
  [G*      | Gsus4*  | G*      | G*     ]
 1 2 3 4   1 2 3 4   1 2 3 4   1 2 3 4

[G]Joey [/ Em]baby [/ C]don't get [/ D]crazy [/]
[G]Detours [/ Em]fences [/ C]I get [/]de[D]fensive [/]

I know you've [G]heard it all before [/]
So I don't [Em]say it anymore [/]
I just stand [C]by and let you [/]fight your secret [D]war 

[D]And though I [G]used to wonder why [/]
I used to [Em]cry till I was dry [/]
Still some[C]times I get a [D]strange pain in[Em]side [/]
Oh, [C]Joey if you're [D]hurting so am [G]I [G Gsus4]
[G G Gsus4]

[G]Joey, [/ Em]honey, [/ C]I got [/]the [D]money [/]
[G]All is [/]for[Em]given, [/ C]listen, [/ D]listen [/]

And if I [G]seem to be confused [/]
I didn't [Em]mean to be with you [/]
And when you [C]said I scared you 
Well I [C]guess you scared me [D]too [/]

But we got [G]lucky once before [/]
And I don't [Em]wanna close the door [/]
And if you're [C]somewhere out there 
[D]Passed out on the [Em]floor [/]
Oh [C]Joey I'm not [D]angry any[G]more [G Gsus4 | G G Gsus4]

      [G*      | Gsus4*  | G*      | G*     ]
SOLO  [G / | Em / | C / | D /]

And if I [G]seem to be confused [/]
I didn't [Em]mean to be with you [/]
And when you [C]said I scared you [/]
Well I guess you scared me [D]too [/]

But if it's [G]love you're lookin’ for [/]
Then I can [Em]give a little more [/]
And if you're [C]somewhere drunk
And [D]passed out on the [Em /]floor 
Oh [C]Joey I'm not [D]angry any[G]more [G Gsus4]
[G G]Angry [Gsus4]any [G]more [G Gsus4]
[G G]Angry [Gsus4]any [G*]more
-----  
#+begin_chorus

#+end_chorus
-----

* Lyrics
