#+TITLE: Please Come Home For Christmas by The Eagles (2)
#+BOOK: Christmas 2020
#+PAGE: 
#+OPTIONS: \n:t toc:nil num:nil ^:nil f:nil *:nil ':nil p:nil author:nil
#+EXPORT_FILE_NAME: _export
#+HTML_HEAD_EXTRA: <style>.chorus{margin-left: 1em;} body{font-family: 'Courier New' !important;}</style>

* Chord Chart
INTRO
e|-3-----------------------|-------------------------|
B|-------------3-----------|-5-----------------------|
G|-------------------------|-------------4-----------|
D|-------------------------|-------------------------|
A|-------------------------|-------------------------|
E|-------------------------|-------------------------|
   1 + 2 + 3 + 4 + 5 + 6 +   1 + 2 + 3 + 4 + 5 + 6 +

Bells will be [G]ringing [Gmaj7]this sad, sad [G7]news [ / ] 
Oh what a [C]Christmas [ / ]to have the [A7]blues [ / ] 
My baby's [G]gone [C]I have no [_G_]friends[_G/F#_ | Em] 
To wish me [A7]greetings [ / ]once [D]again [Daug] 
 
Crowds will be [G]singing [Gmaj7]"Silent [G7]Night" [ / ] 
Christmas [C]carols [ / ]by candle[A7]light [ / ]
Please come home for [G]Christmas
[C]Please come home for [_G_]Christmas [_G/F#_ | Em]
If not for [A7]Christmas, [D7]by New Year's [G]night [G7]
 
#+begin_chorus
Friends and [C]relations [ / ]send [Cm]salutations [ / ]
[G]Sure as the [Daug]stars shine a[G]bove [G7]
But this is [C]Christmas, [ / ]yeah, Christmas [Cm]my dear [ / ]
The time of [A7]year to be
[A7]With the one you [D*]love PICKED [Daug*]
#+end_chorus 

So won't you [G]tell me [Gmaj7]you'll never more [G7]roam [ / ]
Christmas and [C]New Year [ / ]will find you [A7]home [ / ] 
There'll be no more [G]sorrow, [B7]no grief and [_Em_]pain [_Em7_]
[_C_]And [_C#dim_]I'll be [_G_]happy[_Em_]
[_A7_]Happy [_D7_]once a[G]gain [Daug*]
 
SOLO [G Gmaj7 | G7 / | C / | A7 /]

Ooh, There'll be no more [G]sorrow
[B7]No grief and [_Em_]pain [_Em7_]
[_C_]And [_C#dim_]I'll be [_G_]happy [_Em_]
At [A7*]Christmas [D7*]once a[G*]gain [C* C* C* G*]
 
END ON INTRO RIFF

#+begin_chorus

#+end_chorus

* Lyrics
  
-----
#+begin_chorus

#+end_chorus
-----

